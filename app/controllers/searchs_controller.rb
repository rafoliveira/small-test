require 'search'
class SearchsController < ApplicationController
  def search
    search_engine = Search.engine_for(params[:e])
    @search_results = Search.new(search_engine).find_results_for(params[:q])
    respond_to do |format|
      format.html
      format.json { render json: { results: @search_results } }
    end
  end
end
