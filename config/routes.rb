Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'searchs#new'
  get '/search', to: 'searchs#search', as: :search
end
