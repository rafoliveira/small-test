class Search::BothEngine
  def find(query)
    Search::GoogleEngine.new.find(query) + Search::BingEngine.new.find(query)
  end
end
