class Search::GoogleEngine
  def find(query)
    return [] if query.blank?

    results = GoogleSearch.new(q: query, serp_api_key: ENV['SERP_API_KEY']).get_hash
    results[:organic_results].map { |a| Search::Result.new(a[:title], a[:link]) }
  end
end
