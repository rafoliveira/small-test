require 'http'

class Search::BingEngine
  def find(query)
    return [] if query.blank?

    response = HTTP.headers('Ocp-Apim-Subscription-Key' => ENV['BING_KEY'])
      .get("https://api.bing.microsoft.com/v7.0/search?q=#{query}")

    process_response(response)
  end

  private

  def process_response(response)
    json = JSON.parse(response)
    json['webPages']['value'].map { |p| Search::Result.new(p['name'], p['url']) }
  end
end
