
class Search
  module ENGINES
    BOTH = :both
    GOOGLE = :google
    BING = :bing
  end

  Result = Struct.new(:title, :link)

  attr_accessor :engine

  def self.engine_for(engine)
    engine_name = engine || ENGINES::BOTH
    "Search::#{engine_name.capitalize}Engine".constantize.new
  end

  def initialize(engine)
    @engine = engine
  end

  def find_results_for(query)
    @engine.find(query)
  end
end
