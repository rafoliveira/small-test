require 'test_helper'

class DumbEngine
  def find(query)
    return [] unless query == 'test'

    [{ title: 'some title', text: 'text' }]
  end
end

class SearchTest < ActiveSupport::TestCase
  test 'instantiates the engine properly' do
    google_engine = Search.engine_for(Search::ENGINES::GOOGLE)
    bing_engine = Search.engine_for(Search::ENGINES::BING)
    both_engine = Search.engine_for(Search::ENGINES::BOTH)

    assert_equal Search.new(google_engine).engine.class, Search::GoogleEngine
    assert_equal Search.new(bing_engine).engine.class, Search::BingEngine
    assert_equal Search.new(both_engine).engine.class, Search::BothEngine
  end

  # TODO: write better names for the tests
  test '#find_results_for' do
    engine = DumbEngine.new

    results = Search.new(engine).find_results_for('test')
    assert_equal results[0][:title], 'some title'
    assert_equal results[0][:text], 'text'
  end
end
