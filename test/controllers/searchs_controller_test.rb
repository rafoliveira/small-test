class SearchControllerTest < ActionDispatch::IntegrationTest
  test 'it should respond to ajax request' do
    get search_path, as: :json
    assert_equal 'application/json', @response.media_type
  end

  test 'it should respond to html request' do
    get search_path
    assert_equal 'text/html', @response.media_type
  end
  # Context google
  test 'search for coffee using google' do
    get search_path, params: { q: 'coffee', e: 'google' }, as: :json

    items = response.parsed_body['results']
    refute_empty items
  end

  # Context bing
  test 'return empty result if no query is passed' do
    get search_path, params: { q: '', e: 'bing' }, as: :json

    items = response.parsed_body['results']
    assert_empty items
  end

  test 'search for coffee using bing' do
    get search_path, params: { q: 'coffee', e: 'bing' }, as: :json

    items = response.parsed_body['results']
    refute_empty items
  end


end
